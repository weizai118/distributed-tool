package cache.impl;

import cache.CacheClient;
import client.DtClientAware;

import java.util.Collection;
import java.util.Map;

/**
 * Created by xuan on 2018/8/12.
 */
public class DefaultCacheClient extends DtClientAware implements CacheClient {

    @Override
    public String get(String key) {
        return getClient().get(key);
    }

    @Override
    public Map<String, String> getBulk(Collection<String> keys) {
        return getClient().getBulk(keys);
    }

    @Override
    public void set(String key, String value, int seconds) {
        getClient().set(key, value, seconds);
    }

    @Override
    public void setBulk(Map<String, String> map, int seconds) {
        getClient().setBulk(map, seconds);
    }

    @Override
    public void del(String key) {
        getClient().del(key);
    }

    @Override
    public void delBulk(Collection<String> keys) {
        getClient().delBulk(keys);
    }

}
