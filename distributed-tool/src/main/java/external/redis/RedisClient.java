package external.redis;

import client.AbstractDtClient;
import common.MapUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

import java.util.Collection;
import java.util.Map;

/**
 * Created by xuan on 2018/8/10.
 */
public class RedisClient extends AbstractDtClient {

    private String host;

    private int port;

    private String auth;

    private Jedis jedis;

    @Override
    public void init() {
        jedis = new Jedis(host, port);
        if (null != auth) {
            jedis.auth(auth);
        }
    }

    @Override
    public void del(String key) {
        jedis.del(key);
    }

    @Override
    public void delBulk(Collection<String> keys) {
        Pipeline pip = jedis.pipelined();
        keys.forEach(pip::del);
        pip.sync();
    }

    @Override
    public void set(String key, String value, int seconds) {
        jedis.setex(key, seconds, value);
    }

    @Override
    public void setBulk(Map<String, String> map, int seconds) {
        Pipeline pip = jedis.pipelined();
        map.forEach((key, value) -> pip.setex(key, seconds, value));
        pip.sync();
    }

    @Override
    public Long setIfAbsent(String key, String value) {
        return jedis.setnx(key, value);
    }

    @Override
    public String get(String key) {
        return jedis.get(key);
    }

    @Override
    public Map<String, String> getBulk(Collection<String> keys) {
        Pipeline pip = jedis.pipelined();

        Map<String, Response<String>> key2ResponseMap = MapUtil.newHashMap(keys.size());
        keys.forEach(key -> key2ResponseMap.put(key, pip.get(key)));
        pip.sync();

        Map<String, String> key2ValueMap = MapUtil.newHashMap(keys.size());
        key2ResponseMap.forEach((key, response) -> key2ValueMap.put(key, response.get()));
        return key2ValueMap;
    }

    @Override
    public Long incrByLong(String key, long l) {
        return jedis.incrBy(key, l);
    }

    @Override
    public Double incrByDouble(String key, double d) {
        return jedis.incrByFloat(key, d);
    }

    @Override
    public Long decrByLong(String key, long l) {
        return jedis.decrBy(key, l);
    }

    @Override
    public Boolean exists(String key) {
        return jedis.exists(key);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public Jedis getJedis() {
        return jedis;
    }

    public void setJedis(Jedis jedis) {
        this.jedis = jedis;
    }

}
