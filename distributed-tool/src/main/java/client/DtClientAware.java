package client;

/**
 * Created by xuan on 2018/8/12.
 */
public abstract class DtClientAware {

    private DtClient client;

    public DtClient getClient() {
        return client;
    }

    public void setClient(DtClient client) {
        this.client = client;
    }
}
