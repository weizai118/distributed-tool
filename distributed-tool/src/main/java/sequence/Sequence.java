package sequence;

/**
 * 序列号组件
 * Created by xuan on 2018/8/12.
 */
public interface Sequence {

    /**
     * 获取序列号，name表示某一种业务名称，例如：获取订单id，可以使用orderIdBiz等，同一个业务name内序列化不重复
     *
     * @param name 业务名称
     * @return 序列号
     */
    Long nextId(String name);
}
