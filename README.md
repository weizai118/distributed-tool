# distributed-tool

#### 项目介绍
一个分布式常用工具组件。例如：分布式缓存、分布式序列号生成器、分布式锁、分布式订阅发布等等。

#### 软件架构
目前项目分两个module，distributed-tool和distributed-tool-test。前者是核心源码。后者是测试代码。<br/>
distributed-tool主要分如下组件：<br/>
1. client  所有的核心组件依赖模块，主要提供了最简单的分布式set、get、del、incre等操作。（V1.0支持）<br/>
2. cache   简单的分布式缓存模块。（V1.0支持）<br/>
3. sequence  分布式序列号生成组件。（V1.0支持）<br/>

#### 版本更新

1. V1.0 支持分布式缓存、分布式序列号生成组件<br/>

#### Maven引用
<pre><code>
&lt;dependency>
    &lt;groupId>com.xuanner&lt;/groupId>
    &lt;artifactId>distributed-tool&lt;/artifactId>
    &lt;version>1.0&lt;/version>
&lt;/dependency>
</pre></code>

#### 使用说明

（1）核心Client使用（一般不建议直接使用，但是后面的所有分布式组件都基于他来开发的，目前使用了Redis来做实现方案）<br/>

+ 构建DtClient
<pre><code>
protected DtClient buildClient() {
    RedisClient redisClinet = new RedisClient();
    redisClinet.setHost("localhost");
    redisClinet.setPort(1234);
    redisClinet.setAuth("1234");
    redisClinet.init();
    return redisClinet;
}
</pre></code>

+ 代码使用
<pre><code>
protected DtClient buildClient() {
    DtClient client = buildClient();
    client.set("key", "value", 10);
    client.get("key");
    client.del("key");
    client.exists("key");
}
</pre></code>

（2）分布式缓存工具使用
+ 构建CacheClient
<pre><code>
protected CacheClient buildCacheClient() {
    DefaultCacheClient defaultCacheClient = new DefaultCacheClient();
    defaultCacheClient.setClient(buildClient());
    return defaultCacheClient;
}
</pre></code>

+ 代码使用
<pre><code>
CacheClient cacheClient = buildCacheClient();
cacheClient.set("key", "value", 60);
cacheClient.get("key");
cacheClient.del("key");
cacheClient.setBulk(map, 60);
cacheClient.getBulk(list);
cacheClient.del(list);
</pre></code>

（3）分布式序列号使用
+ 构建Sequence
<pre><code>
protected Sequence buildSequence() {
    DefaultSequence defaultSequence = new DefaultSequence();
    defaultSequence.setClient(buildClient());
    defaultSequence.setStep(1000);//步长，如果需要严格连续序号可以设置：1
    defaultSequence.setStepStart(0);//序列号开始位置
    return defaultSequence;
}
</pre></code>

+ 代码使用
<pre><code>
Sequence sequence = buildSequence();
sequence.nextId();
</pre></code>

#### 后续支持功能
1. 分布式订阅发布（通知功能）
2. 分布式锁

#### 联系方式
1. 姓名：徐安
2. 邮箱:javaandswing@163.com
3. QQ：349309307
4. 个人博客：xuanner.com