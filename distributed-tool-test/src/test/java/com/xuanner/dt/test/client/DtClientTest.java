package com.xuanner.dt.test.client;

import client.DtClient;
import com.xuanner.dt.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 核心Client
 * Created by xuan on 2018/8/11.
 */
public class DtClientTest extends BaseTest {

    private final static String TEST_KEY     = "TEST_KEY";
    private final static String TEST_VAL     = "TEST_VAL";
    private final static int    TEST_TIMEOUT = 5;

    @Test
    public void testSetGetDel() {
        DtClient client = buildClient();

        //单个操作
        //set
        client.set(TEST_KEY, TEST_VAL, TEST_TIMEOUT);
        //exists
        Assert.assertTrue(client.exists(TEST_KEY));
        //get
        Assert.assertTrue(TEST_VAL.equals(client.get(TEST_KEY)));
        //del
        client.del(TEST_KEY);
        Assert.assertTrue(!client.exists(TEST_KEY));

        //批量操作
        List<String> kList = new ArrayList<>();
        kList.add("k1");
        kList.add("k2");

        Map<String, String> vMap = new HashMap<>();
        vMap.put("k1", "v1");
        vMap.put("k2", "v2");

        //setBulk
        client.setBulk(vMap, TEST_TIMEOUT);
        //exists
        Assert.assertTrue(client.exists("k1"));
        Assert.assertTrue(client.exists("k2"));
        //getBulk
        Map<String, String> map = client.getBulk(kList);
        Assert.assertTrue("v1".equals(map.get("k1")));
        Assert.assertTrue("v2".equals(map.get("k2")));
        //delBulk
        client.delBulk(kList);
        Assert.assertTrue(!client.exists("k1"));
        Assert.assertTrue(!client.exists("k2"));
    }

    /**
     * 测试过期
     */
    @Test
    public void testExpired() {
        DtClient client = buildClient();

        client.set(TEST_KEY, TEST_VAL, TEST_TIMEOUT);

        //有效期内
        Assert.assertTrue(client.exists(TEST_KEY));
        Assert.assertTrue(TEST_VAL.equals(client.get(TEST_KEY)));

        sleep(TEST_TIMEOUT + 1);

        //过期了
        Assert.assertTrue(!client.exists(TEST_KEY));
        Assert.assertTrue(null == client.get(TEST_KEY));
    }

}
