package com.xuanner.dt.test;

import cache.CacheClient;
import cache.impl.DefaultCacheClient;
import client.DtClient;
import external.redis.RedisClient;
import sequence.Sequence;
import sequence.impl.DefaultSequence;

/**
 * Created by xuan on 2018/8/12.
 */
public class BaseTest {

    /**
     * 核心Client
     *
     * @return
     */
    protected DtClient buildClient() {
        RedisClient redisClinet = new RedisClient();
        redisClinet.setHost("localhost");
        redisClinet.setPort(1234);
        redisClinet.setAuth("1234");
        redisClinet.init();
        return redisClinet;
    }

    /**
     * 序列号生成组件
     *
     * @return
     */
    protected Sequence buildSequence() {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setClient(buildClient());
        defaultSequence.setStep(1000);
        defaultSequence.setStepStart(0);
        return defaultSequence;
    }

    protected CacheClient buildCacheClient() {
        DefaultCacheClient defaultCacheClient = new DefaultCacheClient();
        defaultCacheClient.setClient(buildClient());
        return defaultCacheClient;
    }

    protected void sleep(int time) {
        try {
            Thread.sleep(time * 1000);
        } catch (Exception e) {
            //Ingore
        }
    }

}
